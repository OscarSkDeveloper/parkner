(function() {
    'use strict';

    angular
        .module('lot.listing.service', [])
        .service('LotListingService', LotListingService);

    function LotListingService() {
        var service = this;

        var lots = [{
            address: "Av. Juan de Arona 600, San Isidro",
            lastPayment: "30/12/2017",
            nextDeb: "30/01/2018",
            expDate: "30/04/2018"
        }, {
            address: "Av. Pardo y Aliaga 1200, San Isidro",
            lastPayment: "30/12/2017",
            nextDeb: "30/01/2018",
            expDate: "30/04/2018"
        }, {
            address: "Av. Juan de Arona 600, San Isidro",
            lastPayment: "30/12/2017",
            nextDeb: "30/01/2018",
            expDate: "30/04/2018"
        }, {
            address: "Av. Juan de Arona 600, San Isidro",
            lastPayment: "30/12/2017",
            nextDeb: "30/01/2018",
            expDate: "30/04/2018"
        }, {
            address: "Av. Juan de Arona 600, San Isidro",
            lastPayment: "30/12/2017",
            nextDeb: "30/01/2018",
            expDate: "30/04/2018",
        }];

        service.getLots = function() {
            return lots;
            /*return $http
                .get(API_BASE_PATH + '/lots')
                .then(function(response) {
                    return response;
                }, function(error) {
                    return error;
                });*/
        };
    }

})();
