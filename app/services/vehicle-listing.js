(function() {
    'use strict';

    angular
        .module('vehicle.listing.service', [])
        .service('VehicleListingService', VehicleListingService);

    VehicleListingService.$inject = ['$http', 'API_BASE_PATH'];

    function VehicleListingService($http, API_BASE_PATH) {
        var service = this;

        var vehicles = [{
            placa: "ABC123",
            marca: "FORD",
            modelo: "Mustang",
            color: "Negro",
            ano: "2015"
        }, {
            placa: "ABC456",
            marca: "Toyota",
            modelo: "Tacoma",
            color: "Café",
            ano: "2017"
        }, {
            placa: "ABC789",
            marca: "Nissan",
            modelo: "Frontier",
            color: "Gris",
            ano: "2012"
        }, {
            placa: "BCD123",
            marca: "GMC",
            modelo: "Suburban",
            color: "Negro",
            ano: "2017"
        }, {
            placa: "BCD123",
            marca: "Volvo",
            modelo: "S40",
            color: "Blanco",
            ano: "2012"
        }];

        service.getVehicles = function() {
            return vehicles;
            /*TODO: 
            - http GET VEHICLES endpoint
            */
            /*return $http
                .get(API_BASE_PATH + '/vehicles')
                .then(function(response) {
                    return response;
                }, function(error) {
                    return error;
                });*/
        }
    }

})();
