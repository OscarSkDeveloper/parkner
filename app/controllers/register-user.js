(function() {
    'use strict';

    angular
    .module('register.user.controller', ['ui.router'])
    .config(function($stateProvider) {
        $stateProvider
        .state('register', {
            url: '/register',
            templateUrl: 'app/views/register-user.html',
            controller: 'RegisterUserController as vm'
        });
    })
    .controller('RegisterUserController', RegisterUserController);

    RegisterUserController.$inject = ['RegisterUserService'];

    function RegisterUserController(RegisterUserService) {
        var vm = this;

        vm.register = function(form) {
            console.log('**REGISTER: ', form);
            /*TODO: Call RegisterUserService and http POST form*/
            RegisterUserService.register(form)
            .then(function(response) {
                console.log('Response: ', response);
            }, function(error) {
                console.log('Error: ', error);
            });
        }
    }

})();