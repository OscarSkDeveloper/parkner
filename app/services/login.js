(function() {
    'use strict';

    angular
        .module('login.service', [])
        .factory('LoginService', LoginService);

    LoginService.$inject = ['$http', 'API_BASE_PATH']

    function LoginService($http, API_BASE_PATH) {
        var service = {};

        service.login = function(credentials) {
            return $http
                .post(API_BASE_PATH + '/login', credentials)
                .then(function(response) {
                    /*TODO: 
                    - http POST credentials to login endpoint
                    - Store token in localstorage
                    - Create client side Session
                    */
                    return response;
                }, function(error) {
                    /*TODO: 
                    - Display error
                    */
                    return error;
                });
        }

        return service;
    }

})();
