(function() {
    'use strict';

    angular
        .module('register.lot.service', [])
        .factory('RegisterLotService', RegisterLotService);

    RegisterLotService.$inject = ['$http', 'API_BASE_PATH'];

    function RegisterLotService($http, API_BASE_PATH) {
        var service = {};

        service.register = function(form) {
            return $http
                .post(API_BASE_PATH + '/lot', form)
                .then(function(response) {
                    /*TODO: 
                    - http POST credentials to login endpoint
                    - Store token in localstorage
                    - Create client side Session
                    */
                    return response;
                }, function(error) {
                    /*TODO: 
                    - Display error
                    */
                    return error;
                });
        }

        return service;
    }

})();
