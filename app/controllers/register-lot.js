(function() {
    'use strict';

    angular
        .module('register.lot.controller', ['ui.router'])
        .config(function($stateProvider) {
            $stateProvider
                .state('auth.lot-registration', {
                    url: '/lot-registration',
                    templateUrl: 'app/views/register-lot.html',
                    controller: 'RegisterLotController as vm'
                });
        })
        .controller('RegisterLotController', RegisterLotController);

    RegisterLotController.$inject = ['RegisterLotService'];

    function RegisterLotController(RegisterLotService) {
        var vm = this;

        vm.register = function(form) {
            console.log('**REGISTER LOT: ', form);
            /*TODO: Call RegisterLotService and http POST form*/
            RegisterLotService.register(form)
                .then(function(response) {
                    console.log('Response: ', response);
                }, function(error) {
                    console.log('Error: ', error);
                });
        }
        /*TODO: Call RegisterLotService and http POST form*/
        // Corregido.
    }

})();
