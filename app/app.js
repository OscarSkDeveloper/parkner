(function() {
    'use strict';

    angular
        .module('app', [
            'common',
            'auth0.auth0',
            // USER
            'login.controller',
            'login.service',
            'register.user.controller',
            'register.user.service',
            'help.controller',
            // LOT
            'register.lot.controller',
            'register.lot.service',
            'lot.listing.controller',
            'lot.listing.service',
            'lot.component',
            // VEHICLE
            'vehicle.listing.controller',
            'vehicle.listing.service',
            'vehicle.component',
            // AUTH
            'auth.service',
            'home.controller',
            'parent.controller',
            'app.current'
        ])
        .config(config)
        .run(run);

    config.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'angularAuth0Provider'];

    function config($stateProvider, $locationProvider, $urlRouterProvider, angularAuth0Provider) {
        // Initialization for the angular-auth0 lib
        angularAuth0Provider.init({
            clientID: 'XThFIZAPCp7qYEGAmL8qr9N7pyjwBQJO',
            domain: 'celozanor.auth0.com',
            responseType: 'token id_token',
            audience: 'https://celozanor.auth0.com/userinfo',
            redirectUri: 'http://localhost:5000/#/home',
            scope: 'openid'
        });

        $urlRouterProvider.otherwise('/');
        $locationProvider.hashPrefix('');
    }

    run.$inject = ['AuthService', '$transitions'];

    function run(AuthService, $transitions) {
        AuthService.handleAuthentication();

        $transitions.onStart({ to: 'auth.**' }, function(trans) {
            var auth = trans.injector().get('AuthService')
            if (!auth.isAuthenticated()) {
                // User isn't authenticated. Redirect to a new Target State
                return trans.router.stateService.target('login');
            }
        });
    }

})();
