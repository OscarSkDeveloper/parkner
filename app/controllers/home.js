(function() {
    'use strict';

    angular
        .module('home.controller', [])
        .config(function($stateProvider) {
            $stateProvider
                .state('home', {
                    url: '/home',
                    templateUrl: 'app/views/home.html',
                    controller: 'HomeController as vm'
                });
        })
        .controller('HomeController', HomeController);

    function HomeController() {}

})();
