(function() {
    'use strict';

    angular
        .module('help.controller', ['ui.router'])
        .config(function($stateProvider) {
            $stateProvider
                .state('auth.help', {
                    url: '/help',
                    templateUrl: 'app/views/help.html',
                    controller: 'HelpController as vm'
                });
        })
        .controller('HelpController', HelpController);

    function HelpController() {
        var vm = this;
    }
})();
