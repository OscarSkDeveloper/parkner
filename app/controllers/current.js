(function() {
    'use strict';

    angular
        .module('app.current', [])
        .controller('CurrentUserController', CurrentUserController);

    CurrentUserController.$inject = ['AuthService'];

    function CurrentUserController(AuthService) {
        var vm = this;
        vm.isAuthenticated = AuthService.isAuthenticated;
        vm.login = AuthService.login;
        vm.logout = AuthService.logout;
    }
})();
