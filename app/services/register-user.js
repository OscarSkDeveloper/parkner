(function() {
    'use strict';

    angular
        .module('register.user.service', [])
        .factory('RegisterUserService', RegisterUserService)

    RegisterUserService.$inject = ['$http', 'API_BASE_PATH'];

    function RegisterUserService($http, API_BASE_PATH) {
        var service = {};

        service.register = function(form) {
            return $http
                .post(API_BASE_PATH + '/register', form)
                .then(function(response) {
                    /*TODO: 
                    - http POST credentials to login endpoint
                    - Store token in localstorage
                    - Create client side Session
                    */
                    return response;
                }, function(error) {
                    /*TODO: 
                    - Display error
                    */
                    return error;
                });
        }

        return service;
    }

})();
