(function() {
    'use strict';

    angular
        .module('auth.service', [])
        .service('AuthService', AuthService);

    AuthService.$inject = ['$state', 'angularAuth0', '$timeout'];

    function AuthService($state, angularAuth0, $timeout) {

        function login() {
            angularAuth0.authorize();
        }

        function handleAuthentication() {
            angularAuth0.parseHash(function(err, authResult) {
                console.log("**authResult: ", authResult);

                if (authResult && authResult.accessToken && authResult.idToken) {
                    setSession(authResult);
                    $state.go('home');
                } else if (err) {
                    $timeout(function() {
                        $state.go('home');
                    });
                    console.log("**AUTH: ", err);
                }

            });
        }

        function setSession(authResult) {
            let expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
            localStorage.setItem('access_token', authResult.accessToken);
            localStorage.setItem('id_token', authResult.idToken);
            localStorage.setItem('expires_at', expiresAt);
        }

        function logout() {
            localStorage.removeItem('access_token');
            localStorage.removeItem('id_token');
            localStorage.removeItem('expires_at');
            $location.path('/');
            window.location.reload();
        }


        function isAuthenticated() {
            let expiresAt = JSON.parse(localStorage.getItem('expires_at'));
            return new Date().getTime() < expiresAt;
        }

        return {
            login: login,
            handleAuthentication: handleAuthentication,
            logout: logout,
            isAuthenticated: isAuthenticated
        }

    }

})();
