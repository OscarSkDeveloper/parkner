(function() {
    'use strict';

    angular
        .module('parent.controller', ['ui.router'])
        .config(function($stateProvider) {
            $stateProvider
                .state('auth', {
                    url: '',
                    abstract: false,
                })
        })
        .controller('ParentController', ParentController);

    function ParentController() {}

})();
