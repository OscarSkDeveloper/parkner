(function() {
    'use strict';

    angular
        .module('common', [])
        .constant('API_BASE_PATH', "/api/v1");

})();
