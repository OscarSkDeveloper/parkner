(function() {
    'use strict';

    angular
        .module('login.controller', ['ui.router'])
        .config(function($stateProvider) {
            $stateProvider.state('login', {
                url: '/login',
                templateUrl: 'app/views/login.html',
                controller: 'LoginController as vm'
            });
        })
        .controller('LoginController', LoginController);

    LoginController.$inject = ['AuthService'];

    function LoginController(AuthService) {
        var vm = this;
        vm.auth = AuthService;
    }

})();
