(function() {
    'use strict';

    angular
        .module('lot.component', [])
        .component('lot', {
            templateUrl: 'app/views/lot.html',
            controllerAs: 'vm',
            bindings: {
                category: '<'
            }
        });

})();
