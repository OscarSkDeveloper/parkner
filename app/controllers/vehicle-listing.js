(function() {
    'use strict';

    angular
        .module('vehicle.listing.controller', ['ui.router'])
        .config(function($stateProvider) {
            $stateProvider
                .state('auth.vehicle-listing', {
                    url: '/vehicle-listing',
                    templateUrl: 'app/views/vehicle-listing.html',
                    controller: 'VehicleListingController as vm',
                    resolve: {
                        vehicles: ['VehicleListingService', function(VehicleListingController) {
                            return VehicleListingController.getVehicles();
                        }]
                    }
                });
        })
        .controller('VehicleListingController', VehicleListingController);

    VehicleListingController.$inject = ['vehicles'];

    function VehicleListingController(vehicles) {
        var vm = this;
        vm.vehicles = vehicles;
    }

})();
