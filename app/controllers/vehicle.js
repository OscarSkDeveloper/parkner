(function() {
    'use strict';

    angular
        .module('vehicle.component', [])
        .component('vehicle', {
            templateUrl: 'app/views/vehicle.html',
            controllerAs: 'vm',
            bindings: {
                category: '<'
            }
        });

})();
