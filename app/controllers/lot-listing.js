(function() {
    'use strict';

    angular
        .module('lot.listing.controller', [])
        .config(function($stateProvider) {
            $stateProvider
                .state('auth.lot-listing', {
                    url: '/lot-listing',
                    templateUrl: 'app/views/lot-listing.html',
                    controller: 'LotListingController as vm',
                    resolve: {
                        lots: ['LotListingService', function(LotListingService) {
                            return LotListingService.getLots();
                        }]
                    }
                });
        })
        .controller('LotListingController', LotListingController);

    LotListingController.$inject = ['lots'];

    function LotListingController(lots) {
        var vm = this;
        vm.lots = lots;
    }

})();
